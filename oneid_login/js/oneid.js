(function ($) {
  Drupal.behaviors.oneid_login = {
    attach: function (context, settings) {
      // Your Javascript code goes here
      OneIdExtern.registerApiReadyFunction(function(){
         var params = Drupal.settings.oneid_login.login_button_params;
         
         if (typeof params == "string"){
             params = OneIdUtil.parseJSON(Drupal.settings.oneid_login.login_button_params);
         }
          
         OneId.$(".oneid_login_ctr").each(function(i,elem){
             OneId.loginButton(this, params); 
         });
         
      });
    }
  };
}(jQuery));