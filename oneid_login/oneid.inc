<?php
/*
 * Copyright 2012  by OneID
 */



function oneid_login_call_repo($method, $data = null) {
  $oneid_server = variable_get("keychain_endpoint");
  $oneid_api_id = variable_get("oneid_api_id");
  $oneid_api_key = variable_get("oneid_api_key");

  $scope = "keychain";
  $ch = curl_init($oneid_server . "/" . $scope . "/" . $method);

  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_USERPWD, $oneid_api_id . ":" . $oneid_api_key);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

  if ($data !== null) {
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
  }
  $json = curl_exec($ch);
  curl_close($ch);
  return json_decode($json, true);
}

function oneid_login_call_validate($data) {
  $validate_data = array(
      "nonces" => $data->nonces,
      "attr_claim_tokens" => $data->attr_claim_tokens,
      "uid" => $data->uid
  );

  return oneid_login_call_repo("validate", $data);
}

function oneid_login_call_make_nonce() {
  return oneid_login_call_repo("make_nonce");
}

function oneid_login_get_login_button_params() {
  return json_encode(array("challenge" =>
                              array("attr" => "personal_info[first_name] personal_info[last_name] personal_info[email]",
                                  "callback" => url('oneid_login/callback', array("absolute" => true))
                              )
                    ));
}

function oneid_login_register_callback() {
  global $user;

  if ($jsonInput == null) {
    $handle = fopen('php://input', 'r');
    $jsonInput = fgets($handle);
  }

  $oneid_json = json_decode($jsonInput, false);

  /*
  * TODO: check here if validated email address is required, and handle it.
  */
  $validate_call_data = oneid_login_call_validate($oneid_json);

  if ($validate_call_data && $validate_call_data["error"] == "success") {
    
    if (variable_get("oneid_require_verified_email",false)){
      // Need to make sure email is validated.
      if (!$validate_call_data["attr_claims"]["email"]){
        return drupal_json_output(array("error" => "OneID validated email address required."));
      }
      
    }

    // If user is already logged into Drupal, use that account
    if ($user->uid) {
      try {

        $account = user_external_load($oneid_json->uid);

        if ($account && $account->uid != $user->uid) {
          // Error out.
          return drupal_json_output(array("error" => "Another Drupal user with this UID exists"));
        } else {
          user_set_authmaps($user, array("authname_" . ONEID_LOGIN_MODULE => $oneid_json->uid));
        }
      } catch (PDOException $e) {
        //Swallow this exception for now, auth map already exists.
      }
      user_save($user);
    } else {
      // Look to see if the OneID link has already happend (and we have a Drupal user for OneID user already).
        $account = user_external_load($oneid_json->uid);
      if ($account && $account->uid) {
        $form_state['uid'] = $account->uid;
        user_login_submit(array(), $form_state);
      } else {
        $email = $oneid_json->attr->personal_info->email;
        $account = user_load_by_mail($email);

        // An account with this email already exists, link it to OneID.
        if ($account->uid) {
          /*
          * TODO: need to wire up a new screen that forces user to login with their Drupal account credentials
          * to prove that they own the account.
          */
          try {
            user_set_authmaps($account, array("authname_" . ONEID_LOGIN_MODULE => $oneid_json->uid));
          } catch (PDOException $e) {
            //Swallow this exception for now, auth map already exists.
          }

          $form_state['uid'] = $account->uid;
          user_login_submit(array(), $form_state);
        }
        // No user with this email exists in our Drupal installation, so we can create one.
        else {
          //Create the account.
          user_external_login_register($oneid_json->uid, ONEID_LOGIN_MODULE);
          $user->mail = $email;
          $user->name = $email;
          user_save($user);
        }
      }
    }
    // Redirect user to front and log it as successful.
    $url = url("<front>");
    drupal_json_output(array("error" => "success", "url" => $url));
  } else {
    drupal_json_output($validate_call_data);
  }
}
