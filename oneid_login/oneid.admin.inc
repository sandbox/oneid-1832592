<?php
/*
 * Copyright 2012  by OneID
 */

function oneid_login_admin_settings_form() {
  $form = array();
  $form['api'] = array(
      '#type' => "fieldset",
      '#title' => "OneID Endpoint info"
  );
  $form['api']['oneid_end_point'] = array(
      '#type' => 'textfield',
      '#title' => t('API endpoint'),
      '#default_value' => variable_get('oneid_end_point', "https://api.oneid.com/"),
  );
  $form['api']['keychain_endpoint'] = array(
      '#type' => 'textfield',
      '#title' => t('Keychain endpoint'),
      '#default_value' => variable_get('keychain_endpoint', "https://keychain.oneid.com/"),
  );
  $form['api']['oneid_api_id'] = array(
      '#type' => 'textfield',
      '#title' => t('API ID'),
      '#default_value' => variable_get('oneid_api_id'),
  );
  $form['api']['oneid_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('API Secret Key'),
      '#default_value' => variable_get('oneid_api_key'),
  );

  $form['user_integration'] = array(
      '#type' => "fieldset",
      '#title' => "OneId User Creation"
  );
  $form['user_integration']['oneid_require_verified_email'] = array(
      '#type' => 'checkbox',
      '#title' => t('Require email attribute to be verified?'),
      '#default_value' => variable_get('oneid_require_verified_email', false),
  );

  return system_settings_form($form);
}
